---
title: "How to Write Documentation in a Code-focused Culture"
images: [/img/2020/docs-evolution.png]
layout: post
tags: documentation
---

In some environments, spending time writing documentation is only perceived as time *not*
writing code. Like technical debt, the pain of documentation debt isn't immediately felt.
And paying it down---or even building wealth---is not a task some leaders
in these environments want to spend time on.

So for engineers like myself who have found themselves wanting to write documentation in these code-focused
cultures, it can feel like we have to hide our screens when we're writing docs,
like we're sneaking in something that shouldn't be there. If this sounds crazy
to you, congratulations! You're working in an environment that includes documentation
in its definition of done, and that's great. But if "check the docs" is a joke
on your team, fear not, there's hope.

There's a major difference between writing docs in the shadows and browsing
social media or planting an Easter egg: *no one will complain once you've done it*,
so long as your "code tasks" are also complete. In fact, I've found that the right
documentation can receive a lot of praise in code-focused cultures, from folks
of all seniorities. And public praise of documentation can begin to turn the tide
on the culture as a whole, because leaders can point to your team's or product's
documentation and say, "I want that too."

***So how do you bootstrap documentation in a code-focused environment?***

When I start in a new team that needs documentation, I follow these steps:

1. **Don't write docs.** Start with this nominal approach, because not writing docs is status quo.
If there is a specific deliverable that your peers or
leadership are waiting for you to complete, it won't be acceptable to hold this
up before you've cracked open the culture. This is true for both new team members
who haven't built up street cred, and for senior contributors who are responsible
for a high-value task. Be cognizant of the amount of slack
you have in the current assignment.

2. **Choose an accessible medium.** When you do find some slack,
you can start thinking about the logistics of your upcoming documentation project.
In your organization that doesn't put much stock
into docs, there may not be an obvious place to put your new content. Confluence, GitHub
or GitLab wikis, readmes or other plain-text docs stored with the code, and PDFs
built with Sphinx can all work.
Just choose something that your teammates and leaders know how to find on their own.
It's ok if this resource is currently undervalued---you're going to add content
that changes that perception---but it's going to be harder for you if the documentation
isn't self-service.

3. **Start with FAQ.** Answering questions with written documentation is my definition
of just-in-time documentation, and my favorite way to start documentation organically.
If you have to answer a question from a colleague or customer anyway, it won't take
very long to add a little formatting, double check grammar, and post it somewhere
your team can access. Just don't spend too much time toying with your medium's macros and
advanced formatting at this point. When you're done, post it in chat or email it
to colleagues who might have, or have to answer, the same question.

There's some time between the last step and the next one. In step 3, you are building out
a useful but *reactive* documentation resource. To get to a *proactive* documentation
resource that can preempt an inquiry, you have to understand how your users
are navigating your product (and therefore where they get stumped),
and how they're approaching the documentation. Listen to
the feedback, and if you receive the same question that you've provided the docs
for already, why couldn't they find it? Would adding tags help? Hierarchy?
Do the section headers match the jargon of your users? Are they just looking
for snippets to copy and paste?

![Evolution of docs: start small, grow, then organize.](/assets/img/docs-evolution.png)

It can be frustrating when you spend time on a written solution that no one reads,
but don't take it personally. Remember, you are introducing a new variable into
the ecosystem. Consider your new documentation an experiment. Measure the
response and course-correct as necessary. Don't proceed until you see some
traction with the FAQ.

{:start="4"}
4. **Abstract, codify, and organize.** Once you know how to build docs that
your users can appreciate, start formalizing the documentation for the components
that yield the most questions. Note that this isn't necessarily the same as the
*most used* components---people
may understand those well, so it won't be perceived as providing much value.
If there's no clear frontrunner, I usually start where the user will: installation and setup.
Provide these new docs when you hand off your product, or when you hear a user
will soon be needing the feature you documented.

5. **Maintain.** Don't let people lose faith in documentation by letting it
become outdated and inaccurate. I've found that the closer you keep the docs to
the code---including in the very same repo---the better chance they have to
stay up to date. When docs reside with the code, both can be updated in a single
pull request, instead of docs being an afterthought.
Don't be afraid to build docs *from the code* if it makes sense.
In any case, review the documentation periodically,
prioritizing sections that document rapidly changing components.

Keep your experiment going and iterate on your documentation like you would
iterate on your product. Share your insights with teams who
want or need to bootstrap their docs. Can engineers write docs in your organization
without feeling anxiety? Are they expected to? Do they *want* to? Hopefully
you begin to see movement on this spectrum.

Finally, remember that some of your teammates and leaders started out in
that code-focused culture. In stressful situations, they may revert back to
a comfortable known state where code resumes its role as the only accepted currency.
That means little to no patience for time spent on the docs.
Read the room, and deliver what your team needs now.

*Resources:*

- [Atlassian Confluence Builder for Sphinx](https://sphinxcontrib-confluencebuilder.readthedocs.io/en/latest/) --
  You *can* build Confluence docs from markup. This was a recent, powerful
  discovery for my team.
